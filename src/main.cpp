
#include <WiFi.h>
#include <sstream>
#include <iomanip>
#include <PubSubClient.h> // MQTT Client


const char*  ssid         = "<<HOTSPOT SSID>>";
const char*  password     = "<<HOTSPOT PASSWORD>>";

const char*  mqtt_server  = "syssec.eemcs.utwente.nl";
const int    mqtt_port    = 1883;
const char*  mqtt_user    = "<<MQTT USER>>";
const char*  mqtt_pass    = "<<MQTT PASSWORD>>";

long         current_temp = 2000;
const int    min_temp     = 1000;
const int    max_temp     = 4000;

WiFiClient   wifi_client;
PubSubClient pubsub_client(wifi_client);
 

void setup() {
    randomSeed(analogRead(0));

    Serial.begin(115200);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.println("Connecting to WiFi..");
    }

    pubsub_client.setServer(mqtt_server, mqtt_port);
}

std::string float_to_string(float value) {
    std::ostringstream stream;
    stream << std::fixed << std::setprecision(2) << value;
    return stream.str();
}

void reconnect() {
    // Loop until we're reconnected
    while (!pubsub_client.connected()) {
        Serial.print("Attempting MQTT connection...");

        // Attempt to connect
        if (pubsub_client.connect("", mqtt_user, mqtt_pass)) {
            Serial.println("connected");
        } else {
            Serial.print("failed, rc=");
            Serial.print(pubsub_client.state());
            Serial.println(" try again in 5 seconds");
            delay(5000);
        }
    }
}

float get_next_temperature_reading() {
    // Fake the sensor data by randomly fluctuate the temperature.
    current_temp += random(-10, 10);

    // Ensure fake temperature is within reasonable bounds.
    if (current_temp < min_temp)
        current_temp = min_temp;
    else if (current_temp > max_temp)
        current_temp = max_temp;

    return (float) current_temp / 100.0f;
}

void publish_temperature_data(float value) {
    // Construct topic string.
    std::string username(mqtt_user, 7);
    std::string topic = "temperature/" + username + "/sensor";

    // Convert temperature to a string message.
    std::string msg = float_to_string(value);

    Serial.print("Publishing temperature data [");
    Serial.print(topic.c_str());
    Serial.print("] ");
    Serial.println(msg.c_str());

    // Publish!
    pubsub_client.publish(topic.c_str(), msg.c_str(), msg.length());
}

void loop() {
    // Reconnect if we lost connection.
    if (!pubsub_client.connected()) {
        reconnect();
    }

    // Read some data and publish it.
    float temperature = get_next_temperature_reading();
    publish_temperature_data(temperature);

    // Update internal loops in mqtt client.
    pubsub_client.loop();

    // Wait for 5s before we read and publish the next sensor value.
    delay(5000);
}
